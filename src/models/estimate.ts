import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export interface Estimate {
	_id?: string;
	tenantId: string;
	invoiceIds: string[];
}

export const EstimateDatabase = model<Document & Estimate>(
	'Estimate',
	new Schema(
		{
			tenantId: Types.ObjectId,
			invoiceIds: [{ type: Types.ObjectId, ref: 'Invoice' }],
		},
		{ minimize: false }
	).index({ tenantId: 1 })
);
