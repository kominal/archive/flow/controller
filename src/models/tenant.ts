import { Document, model, Schema } from '@kominal/lib-node-mongodb-interface';

export interface Tenant {
	_id?: string;
	name: string;
	currency: string;
}

export const TenantDatabase = model<Document & Tenant>(
	'Tenant',
	new Schema(
		{
			name: String,
			currency: String,
		},
		{ minimize: false }
	)
);
