import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export interface Project {
	_id?: string;
	tenantId: string;
	name: string;
}

export const ProjectDatabase = model<Document & Project>(
	'Project',
	new Schema(
		{
			tenantId: Types.ObjectId,
			name: String,
		},
		{ minimize: false }
	).index({ tenantId: 1 })
);
