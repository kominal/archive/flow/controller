import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export interface Category {
	_id?: string;
	tenantId?: string;
	name: string;
}

export const CategoryDatabase = model<Document & Category>(
	'Category',
	new Schema(
		{
			tenantId: Types.ObjectId,
			name: String,
		},
		{ minimize: false }
	).index({ tenantId: 1 })
);
