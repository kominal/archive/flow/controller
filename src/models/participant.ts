import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export interface Participant {
	_id?: string;
	tenantId: string;
	name: string;
	created: Date;
}

export const ParticipantDatabase = model<Document & Participant>(
	'Participant',
	new Schema(
		{
			tenantId: Types.ObjectId,
			name: String,
			created: Date,
		},
		{ minimize: false }
	).index({ tenantId: 1 })
);
