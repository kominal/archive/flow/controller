import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export interface Account {
	_id?: string;
	tenantId: string;
	created: Date;
	name: string;
	internal: boolean;
	participantId?: string;
	type: string;
	bank?: string;
	iban?: string;
	bic?: string;
	creditorId: string;
	email?: string;
	autoImport: boolean;
}

export const AccountDatabase = model<Document & Account>(
	'Account',
	new Schema(
		{
			tenantId: Types.ObjectId,
			created: Date,
			name: String,
			internal: Boolean,
			participantId: { type: Types.ObjectId, ref: 'Participant' },
			type: String,
			bank: String,
			iban: String,
			bic: String,
			creditorId: String,
			email: String,
			autoImport: Boolean,
		},
		{ minimize: false }
	).index({ tenantId: 1 })
);
