import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export interface Transaction {
	_id?: string;
	tenantId: string;
	categoryIds?: string[];
	projectIds?: string[];
	invoiceIds?: string[];
	debtorAccountId: string; //Sender
	creditorAccountId: string; //Receiver
	type: string;
	time: Date;
	amount: number;
	currency: string;
	purpose?: string;
	mandateReference?: string;
}

export const TransactionDatabase = model<Document & Transaction>(
	'Transaction',
	new Schema(
		{
			tenantId: Types.ObjectId,
			categoryIds: [{ type: Types.ObjectId, ref: 'Category' }],
			projectIds: [{ type: Types.ObjectId, ref: 'Project' }],
			invoiceIds: [{ type: Types.ObjectId, ref: 'Invoice' }],
			debtorAccountId: { type: Types.ObjectId, ref: 'Account' },
			creditorAccountId: { type: Types.ObjectId, ref: 'Account' },
			type: String,
			time: Date,
			amount: Number,
			currency: String,
			purpose: String,
			mandateReference: String,
		},
		{ minimize: false }
	).index({ tenantId: 1 })
);
