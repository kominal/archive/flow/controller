import { getEnvironmentString } from '@kominal/lib-node-environment';

export const SERVICE_TOKEN = getEnvironmentString('SERVICE_TOKEN');
export const STACK_NAME = getEnvironmentString('STACK_NAME');
