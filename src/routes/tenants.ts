import { Router } from '@kominal/lib-node-express-router';
import { error } from '@kominal/observer-node-client';
import Axios from 'axios';
import { SERVICE_TOKEN, STACK_NAME } from '../helper/environment';
import { AccountDatabase } from '../models/account';
import { CategoryDatabase } from '../models/category';
import { EstimateDatabase } from '../models/estimate';
import { InvoiceDatabase } from '../models/invoice';
import { ParticipantDatabase } from '../models/participant';
import { ProjectDatabase } from '../models/project';
import { Tenant, TenantDatabase } from '../models/tenant';
import { TransactionDatabase } from '../models/transaction';

export const tenantsRouter = new Router();

tenantsRouter.getAsUser<never, Tenant[], never>('/tenants', async (req, res, userId, payload) => {
	if (!payload) {
		throw new Error('transl.error.missing.payload');
	}

	const responseBody = await TenantDatabase.find({ _id: { $in: Object.keys(payload.tenants) } }).lean<Tenant>();

	return {
		statusCode: 200,
		responseBody,
	};
});

tenantsRouter.getAsUser<{ tenantId: string }, Tenant, never>('/tenants/:tenantId', async (req) => {
	return {
		statusCode: 200,
		responseBody: await TenantDatabase.findOne({ _id: req.params.tenantId }).orFail(new Error('transl.error.notFound.category')),
	};
});

tenantsRouter.postAsUser<never, Tenant, { _id: string }, never>('/tenants', async (req, res, userId) => {
	delete req.body._id;

	const tenant = await TenantDatabase.create(req.body);

	try {
		await Axios.put(
			`http://${STACK_NAME}_user-service:3000/user/${userId}/permissions?tenantId=${tenant._id}`,
			{
				permissions: ['core.user-service.write', 'flow.controller.tenant.settings'],
			},
			{
				headers: {
					Authorization: SERVICE_TOKEN,
					userId,
				},
			}
		);
	} catch (e) {
		await tenant.remove();
		error(`Could not set user permissions: ${JSON.stringify(e?.response?.data)}`);
		throw new Error('transl.error.tenantCreationFailed');
	}

	return {
		statusCode: 200,
		responseBody: {
			_id: tenant._id,
		},
	};
});

tenantsRouter.putAsUser<{ tenantId: string }, Tenant, never, never>(
	'/tenants/:tenantId',
	async (req) => {
		await TenantDatabase.updateMany({ _id: req.params.tenantId }, req.body);
		return {
			statusCode: 200,
		};
	},
	{ permissions: ['flow.controller.tenant.settings'] }
);

tenantsRouter.deleteAsUser<{ tenantId: string }, never, never>('/tenants/:tenantId', async (req, res, userId) => {
	const tenant = await TenantDatabase.findById(req.params.tenantId).orFail(new Error('transl.error.notFound.tenant'));

	try {
		await Axios.delete(`http://${STACK_NAME}_user-service:3000/tenants/${tenant._id}`, {
			headers: {
				Authorization: SERVICE_TOKEN,
			},
		});

		await AccountDatabase.deleteMany({ tenantId: tenant._id });
		await CategoryDatabase.deleteMany({ tenantId: tenant._id });
		await EstimateDatabase.deleteMany({ tenantId: tenant._id });
		await InvoiceDatabase.deleteMany({ tenantId: tenant._id });
		await ParticipantDatabase.deleteMany({ tenantId: tenant._id });
		await TransactionDatabase.deleteMany({ tenantId: tenant._id });
		await ProjectDatabase.deleteMany({ tenantId: tenant._id });
		await tenant.remove();
	} catch (e) {
		error(`Could not delete tenant in user service: ${JSON.stringify(e?.response?.data)}`);
		throw new Error('transl.error.tenantDeletionFailed');
	}

	return {
		statusCode: 200,
	};
});
