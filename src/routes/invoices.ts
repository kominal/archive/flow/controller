import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import { Invoice, InvoiceDatabase } from '../models/invoice';

export const invoicesRouter = new Router();

invoicesRouter.getAsUser<{ tenantId: string }, PaginationResponse<Invoice>, PaginationRequest>('/:tenantId/invoices', async (req) => {
	const responseBody = await applyPagination<Invoice>(InvoiceDatabase, req.query, {
		filter: { tenantId: Types.ObjectId(req.params.tenantId) },
	});

	return {
		statusCode: 200,
		responseBody,
	};
});

invoicesRouter.getAsUser<{ tenantId: string; invoiceId: string }, Invoice, never>('/:tenantId/invoices/:invoiceId', async (req) => {
	return {
		statusCode: 200,
		responseBody: await InvoiceDatabase.findOne({ tenantId: req.params.tenantId, _id: req.params.invoiceId }).orFail(
			new Error('transl.error.notFound.invoice')
		),
	};
});

invoicesRouter.postAsUser<{ tenantId: string }, Invoice & { tenantId: never }, { _id: string }, never>(
	'/:tenantId/invoices',
	async (req) => {
		delete req.body._id;

		const invoice = await InvoiceDatabase.create({ ...req.body, tenantId: req.params.tenantId });

		return {
			statusCode: 200,
			responseBody: {
				_id: invoice._id,
			},
		};
	}
);

invoicesRouter.putAsUser<{ tenantId: string; invoiceId: string }, Invoice & { tenantId: never }, never, never>(
	'/:tenantId/invoices/:invoiceId',
	async (req) => {
		await InvoiceDatabase.updateMany({ _id: req.params.invoiceId }, { ...req.body, tenantId: req.params.tenantId });
		return {
			statusCode: 200,
		};
	}
);

invoicesRouter.deleteAsUser<{ tenantId: string; invoiceId: string }, never, never>('/:tenantId/invoices/:invoiceId', async (req) => {
	await InvoiceDatabase.deleteMany({ _id: req.params.invoiceId, tenantId: req.params.tenantId });
	return {
		statusCode: 200,
	};
});
