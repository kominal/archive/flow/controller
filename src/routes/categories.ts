import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import { Category, CategoryDatabase } from '../models/category';

export const categoriesRouter = new Router();

categoriesRouter.getAsUser<{ tenantId: string }, PaginationResponse<Category>, PaginationRequest>('/:tenantId/categories', async (req) => {
	const responseBody = await applyPagination<Category>(CategoryDatabase, req.query, {
		filter: { tenantId: Types.ObjectId(req.params.tenantId) },
	});

	return {
		statusCode: 200,
		responseBody,
	};
});

categoriesRouter.getAsUser<{ tenantId: string; categoryId: string }, Category, never>('/:tenantId/categories/:categoryId', async (req) => {
	return {
		statusCode: 200,
		responseBody: await CategoryDatabase.findOne({ tenantId: req.params.tenantId, _id: req.params.categoryId }).orFail(
			new Error('transl.error.notFound.category')
		),
	};
});

categoriesRouter.postAsUser<{ tenantId: string }, Category & { tenantId: never }, { _id: string }, never>(
	'/:tenantId/categories',
	async (req) => {
		delete req.body._id;

		const category = await CategoryDatabase.create({ ...req.body, tenantId: req.params.tenantId });

		return {
			statusCode: 200,
			responseBody: {
				_id: category._id,
			},
		};
	}
);

categoriesRouter.putAsUser<{ tenantId: string; categoryId: string }, Category & { tenantId: never }, never, never>(
	'/:tenantId/categories/:categoryId',
	async (req) => {
		await CategoryDatabase.updateMany({ _id: req.params.categoryId }, { ...req.body, tenantId: req.params.tenantId });
		return {
			statusCode: 200,
		};
	}
);

categoriesRouter.deleteAsUser<{ tenantId: string; categoryId: string }, never, never>('/:tenantId/categories/:categoryId', async (req) => {
	await CategoryDatabase.deleteMany({ _id: req.params.categoryId, tenantId: req.params.tenantId });
	return {
		statusCode: 200,
	};
});
