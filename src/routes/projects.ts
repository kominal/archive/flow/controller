import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import { Project, ProjectDatabase } from '../models/project';

export const projectsRouter = new Router();

projectsRouter.getAsUser<{ tenantId: string }, PaginationResponse<Project>, PaginationRequest>('/:tenantId/projects', async (req) => {
	const responseBody = await applyPagination<Project>(ProjectDatabase, req.query, {
		filter: { tenantId: Types.ObjectId(req.params.tenantId) },
	});

	return {
		statusCode: 200,
		responseBody,
	};
});

projectsRouter.getAsUser<{ tenantId: string; projectId: string }, Project, never>('/:tenantId/projects/:projectId', async (req) => {
	return {
		statusCode: 200,
		responseBody: await ProjectDatabase.findOne({ tenantId: req.params.tenantId, _id: req.params.projectId }).orFail(
			new Error('transl.error.notFound.project')
		),
	};
});

projectsRouter.postAsUser<{ tenantId: string }, Project & { tenantId: never }, { _id: string }, never>(
	'/:tenantId/projects',
	async (req) => {
		delete req.body._id;

		const project = await ProjectDatabase.create({ ...req.body, tenantId: req.params.tenantId });

		return {
			statusCode: 200,
			responseBody: {
				_id: project._id,
			},
		};
	}
);

projectsRouter.putAsUser<{ tenantId: string; projectId: string }, Project & { tenantId: never }, never, never>(
	'/:tenantId/projects/:projectId',
	async (req, res, userId) => {
		await ProjectDatabase.updateMany({ _id: req.params.projectId }, { ...req.body, tenantId: req.params.tenantId, ownerId: userId });
		return {
			statusCode: 200,
		};
	}
);

projectsRouter.deleteAsUser<{ tenantId: string; projectId: string }, never, never>('/:tenantId/projects/:projectId', async (req) => {
	await ProjectDatabase.deleteMany({ _id: req.params.projectId, tenantId: req.params.tenantId });
	return {
		statusCode: 200,
	};
});
