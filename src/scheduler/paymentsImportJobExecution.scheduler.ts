import { Scheduler } from '@kominal/lib-node-scheduler';
import { Account, AccountDatabase } from '../models/account';
import { ParticipantDatabase } from '../models/participant';
import { TransactionDatabase } from '../models/transaction';
import { TransactionsImportJobDatabase } from '../models/transactionsImportJob';

export class PaymentsImportJobExecutionScheduler extends Scheduler {
	constructor() {
		super();
	}

	async run(): Promise<void> {
		await TransactionsImportJobDatabase.updateMany({ status: 'RUNNING' }, { status: 'FAILED' });

		while (true) {
			const paymentsImportJob = await TransactionsImportJobDatabase.findOne({ status: 'PLANNED' });

			if (!paymentsImportJob) {
				return;
			}

			const account = await AccountDatabase.findById(paymentsImportJob.accountId);

			if (!account) {
				await paymentsImportJob.updateOne({ status: 'FAILED', reason: 'transl.error.paymentsImportJob.account.missing' });
				continue;
			}

			if (1 == 1) {
				await paymentsImportJob.updateOne({ status: 'RUNNING', started: new Date() });
			} else {
				await paymentsImportJob.updateOne({ status: 'FAILED', reason: 'transl.error.paymentsImportJob.credentials.missing' });
				continue;
			}

			const extractedPayments: any[] = [];

			if (!extractedPayments) {
				await paymentsImportJob.updateOne({ status: 'FAILED', reason: 'transl.error.paymentsImportJob.transactions.missing' });
				continue;
			}

			let successful = 0;
			let failed = 0;

			for (const extractedPayment of extractedPayments) {
				try {
					console.log(extractedPayment);

					const participantData = { tenantId: account.tenantId, name: extractedPayment.participant || '-' };

					let participant =
						(await ParticipantDatabase.findOne(participantData)) ||
						(await ParticipantDatabase.create({
							...participantData,
							created: new Date(),
						}));

					let type = 'OTHER';

					if (extractedPayment.type === 'TRANSFER') {
						type = 'BANK';
					} else if (extractedPayment.type === 'DIRECT_DEBIT') {
						type = 'DIRECT_DEBIT_TARGET';
					} else if (extractedPayment.type === 'CARD') {
						type = 'VIRTUAL';
					}

					const accountData: Partial<Account> = {
						tenantId: account.tenantId,
						name: extractedPayment.participant || '-',
						type,
						participantId: participant._id,
					};

					if (extractedPayment.type === 'TRANSFER') {
						accountData.iban = extractedPayment.iban;
						accountData.bic = extractedPayment.bic;
					} else if (extractedPayment.type === 'DIRECT_DEBIT') {
						accountData.creditorId = extractedPayment.creditorId;
					}

					let participantAccount =
						(await AccountDatabase.findOne(accountData)) ||
						(await AccountDatabase.create({
							...accountData,
							created: new Date(),
							internal: false,
							autoImport: false,
						} as any));

					await TransactionDatabase.updateOne(
						{
							tenantId: account.tenantId,
							amount: Math.abs(extractedPayment.amount),
							currency: extractedPayment.currency,
							purpose: extractedPayment.purpose || '',
							sourceAccountId: extractedPayment.amount < 0 ? account._id : participantAccount._id,
							targetAccountId: extractedPayment.amount < 0 ? participantAccount._id : account._id,
							time: extractedPayment.time,
							type: extractedPayment.type,
							mandateReference: extractedPayment.mandateReference,
						},
						{},
						{ upsert: true }
					);
					successful++;
				} catch (e) {
					console.log(e);
					failed++;
				}
			}

			await paymentsImportJob.updateOne({ status: 'COMPLETED', completed: new Date(), successful, failed });
		}
	}
}
